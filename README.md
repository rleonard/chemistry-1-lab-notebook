# Chemistry Lab Notebook
The entire notebook file is saved as notebook.pdf

Changes made from the start of the assignment until right before the lab are marked in prelab-changes.pdf

Changes made from the start of the in class lab period to the end of the in class lab are marked in inlab-changes.pdf

Changes made from the end of the in class lab until lab submission are marked in postlab-changes.pdf

Each of the marked changes are only marked for the most recent lab.  They will be replaced with the changes for the next lab when the next lab starts.